# -*- coding: utf-8 -*-
 
#
# Extracts NICS data from a given CSV file.
#

import csv
import os
from StateCodeDictionary import StateCodeDictionary

NICS_INFILE_NAME = '../raw_data/rd04_NICS.csv'
NICS_OUTFILE_NAME = '../static/data/data_backgroundchecks_generated.js'

def extract_nics_data_from_file(infile = NICS_INFILE_NAME, outfile = NICS_OUTFILE_NAME):
	""" Extracts NICS data from an existing csv file to website-ready html-snippets.
	
	Filters out data for non-relevant regions (e.g. Puerto Rico). 

	Keyword Arguments:
		infile {str} -- CSV file with NICS data to be processed (default: {NICS_INFILE_NAME})
		outfile {str} -- Name of js file with html-snippets (default: {NICS_OUTFILE_NAME})
	"""
	with open(outfile, 'w+') as outfile:

		infile = csv.reader(open(infile, 'r'))
		data_per_state = {}
		scd = StateCodeDictionary()
		for state in scd.CodeToState:
			data_per_state[state] = []

		for line in infile:
			if 'Year' in line[0] and 'Month' not in line[0]:
				current_year = line[0].split(' ')[1]

			if 'Alabama' in line[0]:
				while True:
					if all(a not in line[0] for a in ['Guam', 'Mariana Islands', 'Puerto Rico', 'Virgin Islands']) and current_year != '1998': 
						data_per_state[scd.getStateCode(line[0])].insert(0, int(line[13].replace('.', ''))) 
					if line[0] == 'Wyoming':
						break
					line = next(infile)

		outfile.write('var backgroundCheckData = {')
		for key in data_per_state:
			outfile.write('\n"' + key + '": ' + str(data_per_state[key]) + ',')

		outfile.seek(-1, os.SEEK_END)
		outfile.truncate()
		outfile.write('\n};')


if __name__ == "__main__":
	extract_nics_data_from_file()