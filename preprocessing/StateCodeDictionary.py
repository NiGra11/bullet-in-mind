
## This class provides the functionality to translate state codes and names. All lines of
## the input file must comply with in the following format: CODE,NAME 			
## An example can be found in dummy*DataConstructor.py 			In short: After the
## construction of an instance of this class you can access the dictionaries as following:
 
## scd = StateCodeDictionary('/your/file.csv')	     	# file name is optional, default is rd01
## stateName = scd.getStateName('US-NY')				# should return 'New York'
## stateCode = scd.getStateCode('New York')				# should return 'US-NY'


class StateCodeDictionary(object):

	# Constructor
	def __init__(self, rawDataFile='../raw_data/rd01_usa_codes.csv'):
		self.StateToCode = {}
		self.CodeToState = {}
		entryCount = 0
		rawData = open(rawDataFile, "r")
		data = rawData.readlines()
		rawData.close()
		for i in range(len(data)):
			lineAsList = data[i].strip().split(',')
			code = lineAsList[0].strip()
			name = lineAsList[1].strip()
			self.CodeToState[code] = name
			self.StateToCode[name] = code
			entryCount+=1
		print "Constructed StateCodeDictionary with " + str(entryCount) + " entries from file " + rawDataFile

	# Getter for the state's name
	def getStateName(self, stateCode):
		if(stateCode in self.CodeToState):
			stateName = self.CodeToState[stateCode]
		else:
			stateName = 'notFound'			
			print "StateCodeDictionary could not find the corresponding state name for " + stateCode + "."
		return stateName

	# Getter for the state's code
	def getStateCode(self, stateName):
		if(stateName in self.StateToCode):
			stateCode = self.StateToCode[stateName]
		else:
			stateCode = 'notFound'			
			print "StateCodeDictionary could not find the corresponding state code for " + stateName + "."
		return stateCode
