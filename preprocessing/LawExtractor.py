import re
import sys
import io
import lxml
from lxml.html.clean import Cleaner
from StateCodeDictionary import StateCodeDictionary

## This class provides better functionality to extract the legal situations per state into a csv- or js-file.

## File name is optional, default for inputFile is rd03 and '../static/data/data_law_generated.csv' for outputFile 
## lEx = LawExtractor('/wikipedia/file.html', './test_v1', True)


class LawExtractor(object):


	wikipediaUrl = "https://en.wikipedia.org/wiki/Gun_laws_in_the_United_States_by_state"
	wikipediaBasic = "https://en.wikipedia.org/wiki"
	wikipediaUrlSingleState = "https://en.wikipedia.org/wiki/Gun_laws_in_"
	startTag = "<h2 class=\"in-block section-heading\" onclick=\"javascript:mfTempOpenSection("
	endTag = "</tr></table></div>"
	referenceToMainArticle = "Main article: <a href=\"/wiki/Gun_laws_in"

	
	# Extracts the state name from the line in which it is specified
	def extractStateNameFromLine(self, line):
		end = line.index("</span>")
		line = line[:end]
		start = line.rfind('>')
		stateName = line[start+1:]
		return stateName
	
	# Removes style and attributes
	def cleanEntry(self, entry):
		startOfEdit = entry.find("<span><a")
		endOfEdit = entry.find("</h2>")
		if(startOfEdit!=-1 and endOfEdit!=-1):
			entry = entry[:startOfEdit] + entry[endOfEdit:]										# Remove 'edit' from h2 
		entry = entry.replace("<th>Handguns</th>", "<th>Hand guns</th>") 						# For layout purpose
		entry = entry.replace("<a href=\"#", "<a href=\"" + LawExtractor.wikipediaUrl + "#") 	# Real wikipedia url instead of concat of our site and wikipedia-suffix
		entry = self.cleaner.clean_html(entry)													# Remove style tags		
		entry = entry.replace(" href=", " target=\"_blank\" href=")								# Open all (external) links in a new tab
		entry = entry.replace(" href=\"/wiki", " href=\"" + LawExtractor.wikipediaBasic)		# wikipedia links to other pages		
		entry = re.sub('class=".*?"','', entry)													# Remove class tags		
		entry = re.sub('width=".*?"','', entry)													# Remove width tags
		entry = re.sub('id=".*?"','', entry)													# Remove id tags
		entry = re.sub('\s*>','>', entry)														# Remove spaces before >
		entry = re.sub('>\s*<','><', entry)														# Remove spaces between ><
		entry = entry.replace("\n", " ")	 													# Replace line break with space	
		return entry


	# Replaces the short link with a full link containing the wikipedia-prefix
	def constructLinkToFullArticle(self, stateName):
		link = "<a target=\"_blank\" href=\""
		link += LawExtractor.wikipediaUrlSingleState + stateName.replace(" ", "_")
		link += "\">Go to full article in Wikipedia</a>"
		return link


	# Constructor
	def __init__(self, inputFile='../raw_data/rd03_Gun laws in the United States by state.html', outputFile='../static/data/data_law_generated', outputAsCsv=True):

		if(outputAsCsv):
			outputFile += ".csv"

		else:
			outputFile += ".js"

		outFile = io.open(outputFile, 'w', encoding="utf-8")

		self.scd = StateCodeDictionary()

		self.cleaner = Cleaner()
		self.cleaner.javascript = True # This is True because we want to activate the javascript filter
		self.cleaner.style = True

		# Header or js stuff
		if(outputAsCsv):
			outFile.write(u"stateCode,htmlRepresentation\n")
		else:
			outFile.write(u"var lawDict = {")

		# Instantiation
		stateName = "notFound"
		stateContent = ""
		counter = 0

		# Processing for each line of the input file
		with io.open(inputFile, "r", encoding="utf-8") as inFile:
			for line in inFile:
				if LawExtractor.startTag in line:
					# Handle cut between two states: prefix of line is end of last state - suffix of line is beginning of new state
					breakPoint = line.index(LawExtractor.startTag)
					if(stateName!="" and self.scd.getStateCode(stateName)!="notFound"):
						# Valid state
						stateCode = self.scd.getStateCode(stateName)
						# Add prefix, clean and write
						stateContent += line[:breakPoint]
						stateContent = self.cleanEntry(stateContent)
						stateContent = stateContent[:-6] + "<br><br><br>" +  self.constructLinkToFullArticle(stateName) + "</div>"
						if(outputAsCsv):						
							outFile.write(stateCode + "," + stateContent +  "\n")
						else:						
							if(counter!=0):
								outFile.write(u",")
							stateContent = stateContent.replace("'", "\\'").replace('"', '\\"')
							outFile.write("\n\t'" + stateCode + "': '" + stateContent + "'")
						counter += 1
					# Start with suffix for next state
					stateContent = line[breakPoint:]
					# Extract next statename
					stateName = self.extractStateNameFromLine(line)
							
				else:
					# Append if relevant content
					if(LawExtractor.referenceToMainArticle not in line):
						stateContent += line
		
		if(not outputAsCsv):						
			outFile.write(u"\n};")

		print "Extracted " + str(counter) + " legal situations from " + inputFile + " to " + outputFile

# The main method just calls the constructor
def main():
	lEx = LawExtractor()


if __name__ == "__main__":
    main()


