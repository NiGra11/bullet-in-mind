import re
import sys
import io
import csv
from StateCodeDictionary import StateCodeDictionary

## This class provides functionality to extract the Stanford MSA Shooting data.

## File names are optional, default for inputFile is rd05 and '../static/data/data_shootings_generated.csv' for outputFile
## shootEx = ShootingsExtractor('/stanfordMSA.csv', './shootings_v1', True)  # Example with custom files creating csv


class ShootingsExtractor(object):


	def __init__(self, inputFile='../raw_data/rd05/mass_shooting_events_stanford_msa_release_06142016.csv', outputFile='../static/data/data_shootings_generated', outputAsCsv=True):
		# Construct StateCodeDictionary 
		self.scd = StateCodeDictionary()
		header = True
		first = True
		counter = 0
		# Init stateToShootings dictionary 
		stateToShooting = {}
		for key in self.scd.CodeToState:
			stateToShooting[key] = "["

		# use specified output
		if(outputAsCsv):
			outputFile += ".csv"
			outFile = open(outputFile, 'w')
			outFile.write(u"shootingsId,latitude,logitude,stateCode,cityName,shootingName,date,killed,injured,description")
		else:
			outputFile += ".js"
			outFile = open(outputFile, 'w')
			outFile.write(u"var shootingsData = [")

		# Processing for each line of the input file
		with open(inputFile, 'rU') as inFile:
			mycsv = csv.reader(inFile)
			for row in mycsv:
				if(row[0]!="" and not header):
					latitude = row[5]
					longitude = row[6]
					stateName = row[4]
					stateCode = self.scd.getStateCode(stateName)
					cityName = row[3]
					shootingName = row[1]
					date = self.convertDate(row[12])
					killed = row[7]
					injured = row[9]
					description = row[11]

					if(self.isInRelevantTimeRange(date)):

						if(outputAsCsv):
							# Writing for csv
							# shootingsId,latitude,logitude,stateCode,cityName,shootingName,date,killed,injured,description
							line = str(counter) + ',' + latitude + ',' + longitude + ',"' + stateCode + '","' + cityName + '","' + shootingName + '",' + date + ',' + killed + ',' + injured + ',"' + description + '"'
							outFile.write("\n" + line)				
						else:
							# Writing for js
							stateToShooting[stateCode] = stateToShooting[stateCode] + str(counter) + ", "
							line = "{id: " + str(counter) + ", latLng: [" + latitude + ", " + longitude + "], city: '" + cityName + "', state: '" + stateCode + "', date: '" + date + "', killed: " + killed + ", injured: " + injured + ", name: '" + shootingName.replace("'", "\\'") + "', description: '" + description.replace("'", "\\'").replace("\n", " ") + "' }"
							if(first):
								outFile.write("\n\t" + line)
								first = False
							else:
								outFile.write(",\n\t" + line)
						counter += 1
				else:
					header = False

		# Write dictionary if necessary
		if(not outputAsCsv):
			outFile.write(u"\n];\n\n\nvar stateToShootingIndexDict = {")

			first = True
			for key in stateToShooting:
				final = stateToShooting[key] + "]"
				final = "'" + key + "': " + final.replace(", ]", "]")
				if(first):
					first = False
					final = "\n\t" + final
				else:
					final = ",\n\t" + final
				outFile.write(final)

			outFile.write(u"\n};")

		print "Extracted " + str(counter) + " shootings from " + inputFile + " to " + outputFile


	# converts data to full character string of yyyy-mm-dd
	def convertDate(self, asString):
		asList = asString.split("/")
		month = int(asList[0])
		day = int(asList[1])
		year = int(asList[2])
		if(year<66):
			year += 2000
		elif(year<100):
			year += 1900
		return "%04d-%02d-%02d" % (year,month,day)

	# Check for relevant time (must be between 1999 and 2016)
	def isInRelevantTimeRange(self, date):
		year = int(date[:4])
		return year>=1999 and year<=2016



def main():
	shootingsEx = ShootingsExtractor(outputAsCsv=True)


if __name__ == "__main__":
    main()


