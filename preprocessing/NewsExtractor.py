 # -*- coding: utf-8 -*-
 
#
# Will extract news from Guardian and NYT APIs, plus abstracts from the Guardian website.
# Queries for "<SHOOTINGNAME>"" and "<CITYNAME> shooting" in the corresponding date span (default is 2 days).
#
# Needs to be run with two API keys, for the Guardian and the NYT:
#       python NewsExtractor.py API_KEY_NYT API_KEY_GUARDIAN
#

import requests
import urllib
import csv
import datetime
import time
import os
from lxml import html
import sys

script_path = os.path.dirname(os.path.abspath( __file__ ))

# --------------- PARAMETERS ---------------
QUERIED_DAY_SPAN = 2
INFILE_NAME_CSV = script_path + '/../static/data/data_shootings_generated.csv'
OUTFILE_NAME_CSV = script_path + '/../static/data/data_news.csv'
OUTFILE_NAME_JS  = script_path + '/../static/data/data_news_clean.js'
OUTFILE_NAME_GUARDIAN_CSV = script_path + '/../static/data/data_news_guardian.csv'
OUTFILE_NAME_GUARDIAN_JS  = script_path + '/../static/data/data_news_guardian_clean.js'
# ------------------------------------------


def extract_nyt_news(api_key, shooting_data_file=INFILE_NAME_CSV, outname_csv = OUTFILE_NAME_CSV, outname_js = OUTFILE_NAME_JS):
    """ Extracts shooting news for a given data file from the NYT API.
    
    Queries for "<SHOOTINGNAME>"" and "<CITYNAME> shooting" from start date in data up until the given date span.
    Saves news as web-ready html-snippets in a js file and in a clean version in a csv file.

    Arguments:
        api_key {str} -- A valid NYT API key
    
    Keyword Arguments:
        shooting_data_file {str} -- CSV file of the shooting data (default: {INFILE_NAME_CSV})
        outname_csv {str} -- Name of the csv file to be written into (default: {OUTFILE_NAME_CSV})
        outname_js {str} -- Name of the js file to be written into  (default: {OUTFILE_NAME_JS})
    """

    with open(shooting_data_file, 'r') as infile, open(outname_csv, 'w+') as outfile_csv, open(outname_js, 'w+') as outfile_js:
        outfile_js.write('var newsData = [')
        outfile_csv.write('newsId,shootingsId,url,headline,abstract\n')
        news_id = 0
        next(infile)
        for entry in csv.reader(infile):
            news_entries = []
            shooting_id = entry[0] 
            shooting_name = entry[5]
            dates = get_formated_dates(entry[6])

            res_json = query_nyt(api_key, shooting_name, dates[0], dates[1])

            if res_json:
                news_entries = [article for article in res_json['response']['docs']]

            # Also query for articles about "<CITYNAME> shooting"
            if entry[4] != '':
                res_json = query_nyt(api_key, entry[4] + " shooting", dates[0], dates[1])
                if res_json:
                    news_entries += [entry for entry in res_json['response']['docs']]

            headers = []

            for article in news_entries:

                header = article['headline']['main']
                if article['document_type'] == 'article' and header not in headers and header != "QUOTATION OF THE DAY" :
                    headers.append(header)
                    res = "\"<div><tr><td><h3><img id='img-nyt' src='../static/images/nyt.png'><a target='_blank' href='" + article['web_url'] + "'>" + header.encode('ascii', 'xmlcharrefreplace').replace("'", "\\'").replace('"', '\\"') + "</a></h3>"
                    try:
                        abstract = article['abstract']
                    except KeyError:
                        try: 
                            abstract = article['snippet']
                        except KeyError:
                            abstract = ''

                    res += "<p>" + abstract.encode('ascii', 'xmlcharrefreplace').replace("'", "\\'").replace('"', '\\"') + "</p></td></tr></div>\""    
                    outfile_js.write('\n{id: ' + shooting_id + ', html: ' + res + '},')
                    outfile_csv.write(str(news_id).encode('utf-8') + ',' + str(shooting_id).encode('utf-8') + ',"' + article['web_url'].encode('utf-8') + '","' + header.encode('utf-8') + '","' + abstract.encode('utf-8') + '"\n')
                    news_id += 1

            if not headers:
                outfile_js.write('\n{id: ' + shooting_id + ', html: "<div><p> No relevant news stories were found. </p></div>"},')

        outfile_js.seek(-1, os.SEEK_END)
        outfile_js.truncate()
        outfile_js.write('\n];')


def query_nyt(api_key, search_term, begin_date, end_date=""):
    """ Gets result for a queried search term from NYT API.
        
    Arguments:
        api_key {str} --  A valid NYT API key
        search_term {str} -- The search term to be queried
        begin_date {str} -- The start date for when to look for articles (format '%Y-%m-%d')
    
    Keyword Arguments:
        end_date {str} -- The end date for when to look for articles (format '%Y-%m-%d'), or empty if all (default: {""})
    
    Returns:
        json object -- JSON-object of the response
    """
    paras = {'api-key': api_key, 'q' : search_term}

    if begin_date != "":
       paras['begin_date'] = begin_date
    if end_date != "":
       paras['end_date'] = end_date

    url = "https://api.nytimes.com/svc/search/v2/articlesearch.json?" + urllib.urlencode(paras);
    print url

    response = requests.get(url)

    if response.status_code != 200:
        print "Can't get news infos. Server responded with status code " + str(response.status_code)
        return {}

    time.sleep(0.5)
    return response.json()


def extract_guardian_news(api_key, shooting_data_file=INFILE_NAME_CSV, outname_csv = OUTFILE_NAME_GUARDIAN_CSV, 
                          outname_js = OUTFILE_NAME_GUARDIAN_JS):
    """ Extracts shooting news for a given data file from the Guardian API.
    
    Queries for "<SHOOTINGNAME>"" and "<CITYNAME> shooting" from start date in data up until the given date span.
    Saves news as web-ready html-snippets in a js file and in a clean version in a csv file.
    
    Arguments:
        api_key {str} -- A valid Guardian API key
    
    Keyword Arguments:
        shooting_data_file {str} -- CSV file of the shooting data  (default: {INFILE_NAME_CSV})
        outname_csv {str} -- Name of the csv file to be written into (default: {OUTFILE_NAME_GUARDIAN_CSV})
        outname_js {str} -- Name of the js file to be written into (default: {OUTFILE_NAME_GUARDIAN_JS})
    """
    
    with open(shooting_data_file, 'r') as infile, open(outname_csv, 'w+') as outfile_csv, open(outname_js, 'w+') as outfile_js:
        outfile_js.write('var newsDataGuardian = [')
        outfile_csv.write('newsId,shootingsId,url,headline,abstract\n')
        news_id = 0
        next(infile)

        for entry in csv.reader(infile):
            news_entries = []
            shooting_id = entry[0] 
            shooting_name = entry[5]
            start_date = datetime.datetime.strptime(entry[6], '%Y-%m-%d')
            end_date = start_date + datetime.timedelta(days=QUERIED_DAY_SPAN)

            start_date = datetime.datetime.strftime(start_date, '%Y-%m-%d')
            end_date = datetime.datetime.strftime(end_date, '%Y-%m-%d')

            res_json = query_guardian(api_key, shooting_name, start_date, end_date)

            if res_json:
                if res_json['response']['results']:
                    news_entries = [entry for entry in res_json['response']['results']]

            # Also query for articles about "<CITYNAME> shooting"
            try:
                if entry[4] != '':
                    res_json = query_guardian(api_key, entry[4] + " shooting", start_date, end_date)
                    if res_json:
                        if res_json['response']['results']:
                            news_entries += [entry for entry in res_json['response']['results']]
            except KeyError:
                pass
        
            headers = []

            for article in news_entries:

                header = article['webTitle'].strip()
                if article['type'] == 'article' and header not in headers:
                    headers.append(header)
                    res = "\"<div><tr><td><h3><img id='img-guardian' src='../static/images/guardian.png'><a target='_blank' href='" + article['webUrl'] + "' float='right' text-align='center'>" + header.encode('ascii', 'xmlcharrefreplace').replace("'", "\\'").replace('"', '\\"') + "</a></h3>"
                    
                    abstract = get_guardian_aricle_abstract(article['webUrl'])

                    res += "<p>" + abstract.encode('ascii', 'xmlcharrefreplace').replace("'", "\\'").replace('"', '\\"') + "</p></td></tr></div>\""    
                    outfile_js.write('\n{id: ' + shooting_id + ', html: ' + res + '},')
                    outfile_csv.write(str(news_id).encode('utf-8') + ',' + str(shooting_id).encode('utf-8') + ',"' + article['webUrl'].encode('utf-8') + '","' + header.encode('utf-8') + '","' + abstract.encode('utf-8') + '"\n')
                    news_id += 1

            if not headers:
                outfile_js.write('\n{id: ' + shooting_id + ', html: "<div><p> No relevant news stories were found. </p></div>"},')

        outfile_js.seek(-1, os.SEEK_END)
        outfile_js.truncate()
        outfile_js.write('\n];')


def get_guardian_aricle_abstract(url):
    """ Extracts an article abstract from a given url to a Guardian news article.
    
    NOTE: This method might not work properly anymore if the Guardian significantly changes
    its website structure. 

    Arguments:
        url {str} -- URL to the respective Guardian news article
    
    Returns:
        str -- The abstract text
    """
    
    response = requests.get(url)
    if response.status_code != 200:
        return ''

    abstract = ''
    tree = html.fromstring(response.content)
    xpath_query = '//div[@class="content__headline-standfirst-wrapper"]//div[@class="content__standfirst"]//text()'
    entries = tree.xpath(xpath_query)
    for entry in entries:
        entry = entry.strip()
        if entry:
            abstract += entry + ' '

    return abstract.strip()


def query_guardian(api_key, search_term, begin_date, end_date=""):
    """ Gets result for a queried search term from Guardian API.
    
    Arguments:
        api_key {str} --  A valid Guardian API key
        search_term {str} -- The search term to be queried
        begin_date {str} -- The start date for when to look for articles (format '%Y-%m-%d')
    
    Keyword Arguments:
        end_date {str} -- The end date for when to look for articles (format '%Y-%m-%d'), or empty if all (default: {""})
    
    Returns:
        json object -- JSON-object of the response
    """

    search_term_query = ""
    for term in search_term.split():
        search_term_query += term + " AND "
    search_term_query = search_term_query[:len(search_term_query)-5]

    paras = {'api-key': api_key, 'q' : search_term_query}

    if begin_date != "":
       paras['from-date'] = begin_date
    if end_date != "":
       paras['to-date'] = end_date

    url = "https://content.guardianapis.com/search?" + urllib.urlencode(paras);
    print url

    response = requests.get(url)

    if response.status_code != 200:
        print "Can't get news infos. Server responded with status code " + str(response.status_code)
        return {}

    time.sleep(0.1)
    return response.json()


def get_formated_dates(input_date_string):
    """ Returns list wth formatted start date and end date.
    
    Excpects a string of format YYYY-M(M)-D(D) (days or months possibly with length 1).
    
    Arguments:
        input_date_string {str} -- String of date of format YYYY-M(M)-D(D)
    """
    start_date = datetime.datetime.strptime(input_date_string, '%Y-%m-%d')
    end_date = start_date + datetime.timedelta(days=QUERIED_DAY_SPAN)
    return [ datetime.datetime.strftime(start_date, '%Y%m%d'), 
             datetime.datetime.strftime(end_date, '%Y%m%d')]

             
def analyse_extracted_news(outfile=OUTFILE_NAME_CSV):
    """ Analyses average, maximum and minimum counts of news found for incidents.
    
    Keyword Arguments:
        outfile {str} -- CSV file name of the extracted news data (default: {OUTFILE_NAME_CSV})
    """
    if not os.path.isfile(outfile):
        print 'No csv news file found.'
        return
    print 'Results for ' + outfile + ':  '
    with open(outfile, 'r') as newsfile:
        next(newsfile)
        metric_dic = {}
        for line in newsfile:
            try:
                shooting_id = line.split(',')[1]
                if shooting_id in metric_dic:
                    metric_dic[shooting_id] += 1
                else:
                    metric_dic[shooting_id] = 1
            except IndexError:
                pass

    print '-------- RESULT --------'
    print 'Number of shootings for which news were found: ' + str(len(metric_dic))
    if metric_dic:
        print 'Average number of news per shooting found: ' + str(float(sum(metric_dic.values()) / max(len(metric_dic), 1)))
        print 'Maximum number of news for a single shooting: ' + str(max(metric_dic.values()))
        print 'Minimum number of news for a single shooting: ' + str(min(metric_dic.values()))


def main():
    """ Excecutes main news extraction workflow.
        NOTE: Calling this will might a while (many http-requests & API constraints)!
    """
    if len(sys.argv) < 3:
        print 'No API keys passed! Needs both NYT and Guardian key for full run.'
    else:
        extract_nyt_news(api_key=sys.argv[1])
        extract_guardian_news(api_key=sys.argv[2])
        analyse_extracted_news(OUTFILE_NAME_CSV)
        analyse_extracted_news(OUTFILE_NAME_GUARDIAN_CSV)


if __name__ == "__main__":
    main()
