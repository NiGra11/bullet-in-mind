# -*- coding: utf-8 -*-

#
# Goes through all collected NYT and Guardian news headlines and abstracts to extract keywords.
# Highlights them in the text on the website (on the News tab) and constructs the ranked list with scores.
# 

from __future__ import unicode_literals
from rake_nltk import Rake
import requests
from lxml import html
import re
from shutil import copyfile
import os
from summa import keywords
import string

script_path = os.path.dirname(os.path.abspath( __file__ ))

# --------------- PARAMETERS ---------------
FILE_NEWS_CSV = script_path + '/../static/data/data_news.csv'
FILE_NEWS_GUARDIAN_CSV = script_path + '/../static/data/data_news_guardian.csv'
FILE_NEWS_JS =  script_path + '/../static/data/data_news.js'
FILE_NEWS_GUARDIAN_JS = script_path + '/../static/data/data_news_guardian.js'
FILE_NEWS_CLEAN_JS = script_path + '/../static/data/data_news_clean.js'
FILE_NEWS_GUARDIAN_CLEAN_JS = script_path + '/../static/data/data_news_guardian_clean.js'
FILE_KEYWORD_LIST = script_path + '/../static/data/data_keyword_list.js'
TOP_N_PHRASES = 200
MAX_WORDS_PER_PHRASE = 2
USE_FULL_TEXT = False
# ------------------------------------------


def extract_keywords(max_words_per_phrase=2, use_full_text=False):
    """ Extracts keyphrases of a certain size with RAKE from news text.
    
    Will extract keyphrases of up to given max length from either the complete news text (a.e. including articles)
    or only the headline + abstracts that are also displayed on the BIM website (recommended for displayable results).
    
    Keyword Arguments:
        max_words_per_phrase {number} -- Maximum phrase length to consider (default: {2})
        use_full_text {bool} -- If False, only consider headlines and abstracts, if True article text as well (default: {False})
    
    Returns:
        list -- A list of tuples of (RAKE-score, phrase)
    """
    r = Rake()
    r.extract_keywords_from_text(collect_news_text(use_full_text))
    phrases = r.get_ranked_phrases_with_scores()
    if max_words_per_phrase == 0:
        return phrases

    filtered_phrases = []
    for phrase in phrases:
        if len(phrase[1].split()) <= max_words_per_phrase:
            filtered_phrases.append(phrase)

    return filtered_phrases


def collect_news_text(use_full_text=False):
    """ Collects all news text data into a single string.
    
    Will either collect text from headline and abstract only (recommended for displayable results)
    or the full news text via a http request to the respective website.
    
    Keyword Arguments:
        use_full_text {bool} -- If False, only consider headlines and abstracts, if True article text as well (default: {False})
    
    Returns:
        str -- The complete text
    """
    text = ''
    translation_table = {ord(char): None for char in string.punctuation}
    
    for file in [FILE_NEWS_CSV, FILE_NEWS_GUARDIAN_CSV]:
        with open(file, 'r') as infile:
            next(infile)
            for line in infile:

                if use_full_text:
                    url = line.decode('utf-8').split(',')[2].strip('"')

                    response = requests.get(url)

                    if file == FILE_NEWS_CSV:
                        xpath_query = '//p[@class="story-body-text story-content"]/text()'
                    else:
                        xpath_query = '//div[@class="content__article-body from-content-api js-article__body"]//text()'
                    
                    if response.status_code == 200:
                        tree = html.fromstring(response.content)
                        entries = tree.xpath(xpath_query)
                        for entry in entries:
                            text += entry.translate(translation_table)
                else:
                    try:
                        text += line.decode('utf-8').split(',')[4].strip('"').translate(translation_table)
                    except IndexError:
                        print ' > INDEX ERROR with following line: '
                        print line

    text = ''.join(e for e in text if e.isalnum() or e in [' ', '-'])
    return text 


def highlight_keywords_in_abstracts(phrases, top_n=TOP_N_PHRASES):
    """ Highlights the top n keywords in prepared html news-snippets.
    
    Arguments:
        phrases {list} -- A list of tuples of (RAKE-score, phrase)
    
    Keyword Arguments:
        top_n {number} -- How many of the top phrases will be highlighted (default: {TOP_N_PHRASES})
    """
    phrases = sorted(phrases, reverse=True)[:top_n]

    for file, file_clean in [(FILE_NEWS_JS, FILE_NEWS_CLEAN_JS), (FILE_NEWS_GUARDIAN_JS, FILE_NEWS_GUARDIAN_CLEAN_JS)]:
        os.remove(file)

        with open(file_clean, 'r') as jsfile:
            with open(file, 'w') as outfile:
                for line in jsfile:
                    for phrase in phrases:
                        indices = [m.start() for m in re.finditer(' ' + phrase[1] + ' ', line)]
                        if len(indices) > 0:
                            highlight_before = "<span title='" + str(phrase[0]) + "' style='color:DodgerBlue'><b>"
                            highlight_after = "</b></span>"
                            indices = indices[:1] + [x+len(highlight_before)+len(highlight_after) for x in indices[1:]]
                            for index in indices:
                                line = line[:index] + highlight_before + line[index:index+len(phrase[1])+1] + highlight_after + line[index+len(phrase[1])+1:]
                    outfile.write(line)


def build_ranked_list(phrases, top_n=TOP_N_PHRASES):
    """ Write a sorted list of the top n phrases into a file.
        
    Arguments:
        phrases {list} -- A list of tuples of (RAKE-score, phrase)
    
    Keyword Arguments:
        top_n {number} -- How many of the top phrases will be highlighted (default: {TOP_N_PHRASES})
    """
    sorted_phrases = sorted(phrases, reverse=True)

    with open(FILE_KEYWORD_LIST, 'w') as outfile:
        outfile.write('var keyphraseList = [')

        for i, phrase in enumerate(sorted_phrases[:top_n]):
            outfile.write('\n{id: ' + str(i) + ', phrase: "' 
                + phrase[1].encode('ascii', 'xmlcharrefreplace').replace("'", "\\'").replace('"', '\\"') 
                + '", score: ' + str(phrase[0]) + '},')

        outfile.seek(-1, os.SEEK_END)
        outfile.truncate()
        outfile.write('\n];')


def main():
    """ Excecutes main extraction workflow. """
    print 'Extracting key phrases...'
    phrases = extract_keywords(max_words_per_phrase=MAX_WORDS_PER_PHRASE, use_full_text=USE_FULL_TEXT)
    print 'Highlighting key phrases in files...'
    highlight_keywords_in_abstracts(phrases)
    print 'Building the ranked list of phrases...'
    build_ranked_list(phrases)


if __name__ == '__main__':
    main()
