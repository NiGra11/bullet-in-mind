\boolfalse {citerequest}\boolfalse {citetracker}\boolfalse {pagetracker}\boolfalse {backtracker}\relax 
\defcounter {refsection}{0}\relax 
\select@language {ngerman}
\defcounter {refsection}{0}\relax 
\contentsline {section}{\numberline {1}Motivation}{1}{section.1}
\defcounter {refsection}{0}\relax 
\contentsline {section}{\numberline {2}Datenquellen}{1}{section.2}
\defcounter {refsection}{0}\relax 
\contentsline {subsection}{\numberline {2.1}Verwendete Datenquellen}{1}{subsection.2.1}
\defcounter {refsection}{0}\relax 
\contentsline {subsection}{\numberline {2.2}Nicht verwendete Datenquellen}{3}{subsection.2.2}
\defcounter {refsection}{0}\relax 
\contentsline {section}{\numberline {3}Verwendete Technologien}{4}{section.3}
\defcounter {refsection}{0}\relax 
\contentsline {subsection}{\numberline {3.1}Preprocessing}{4}{subsection.3.1}
\defcounter {refsection}{0}\relax 
\contentsline {subsection}{\numberline {3.2}Backend}{6}{subsection.3.2}
\defcounter {refsection}{0}\relax 
\contentsline {subsection}{\numberline {3.3}Frontend}{6}{subsection.3.3}
\defcounter {refsection}{0}\relax 
\contentsline {subsection}{\numberline {3.4}Schnittstelle}{6}{subsection.3.4}
\defcounter {refsection}{0}\relax 
\contentsline {subsection}{\numberline {3.5}Organisation und Formales}{6}{subsection.3.5}
\defcounter {refsection}{0}\relax 
\contentsline {section}{\numberline {4}Textanalyse}{7}{section.4}
\defcounter {refsection}{0}\relax 
\contentsline {subsection}{\numberline {4.1}RAKE}{7}{subsection.4.1}
\defcounter {refsection}{0}\relax 
\contentsline {subsection}{\numberline {4.2}Implementierung und Ergebnisse}{8}{subsection.4.2}
\defcounter {refsection}{0}\relax 
\contentsline {section}{\numberline {5}Verwendung von BIM}{9}{section.5}
\defcounter {refsection}{0}\relax 
\contentsline {subsection}{\numberline {5.1}Anwendungsbezogene Aspekte}{9}{subsection.5.1}
\defcounter {refsection}{0}\relax 
\contentsline {subsection}{\numberline {5.2}Technische Aspekte}{10}{subsection.5.2}
\defcounter {refsection}{0}\relax 
\contentsline {section}{\numberline {6}Aufgetretene Probleme}{10}{section.6}
\defcounter {refsection}{0}\relax 
\contentsline {section}{\numberline {7}Fazit \& Ausblick}{11}{section.7}
