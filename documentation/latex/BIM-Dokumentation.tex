\documentclass[a4paper,fontsize=12pt,toc=bib,halfparskip]{scrartcl}

\usepackage[utf8]{inputenc} % utf8 input encoding
\usepackage{lmodern} % font
\usepackage[T1]{fontenc} % 8-Bit output encoding
\usepackage[ngerman]{babel} % This is a (new)german document so we want to have german labels as well
\usepackage[dvipsnames]{xcolor} % refer to some colors by name
\usepackage{graphicx} % able to load more graphic formats
\usepackage[pdfborder={0 0 0},colorlinks,urlcolor=NavyBlue,linkcolor=NavyBlue,citecolor=NavyBlue]{hyperref} % make clickable hyper links in documents
\usepackage[headsepline,plainheadsepline]{scrpage2} % for header and footer layout
\usepackage{nag} % for not using forbidden things referenced in l2tabu
\usepackage{biblatex} % for references
\usepackage{xspace} % inserts missing space at the end of custom commands
\usepackage{subfig} % for subfigures
\usepackage{amsmath} % for cool math stuff
\usepackage{microtype} % for better kerning
\usepackage{hyperref} % for links
\usepackage{float}	% für die exakte Positionierung mit H

\addbibresource{literatur.bib} % database for references
\graphicspath{{./images/}} % sets the default path for images

% header and footer settings
\pagestyle{scrheadings}
\clearscrheadfoot
\newcommand{\veranstaltung}{Praktikum Wissens- \& Contentmanagement WS~2017/2018\xspace}
\newcommand{\toolName}{Bullet in mind - Dokumentation\xspace}
\lohead{\toolName}
\rohead{\headmark}
\setfootsepline{0.1pt}
\ofoot{\textsf{\thepage/\getpagerefnumber{LastPageBeforeRefs}}}

% title page
\renewcommand*{\titlepagestyle}{empty}
\subject{\veranstaltung}
\title{\vspace{1.5cm}Bullet in mind}
\subtitle{Interaktives Tool zur Untersuchung von Daten \linebreak zum Schusswaffenbesitz und -missbrauch \linebreak in den USA zwischen 1999 und 2016\vspace{2cm}}
\author{Felix Helfer \& Nico Graebling}
\date{\today}
\newcommand{\kurz}{Diese Dokumentation erläutert das Tool \textit{Bullet In Mind (BIM)}, welches im Rahmen des Praktikums zum Modul Wissens- und Contentmanagement im Wintersemester 2017/18 an der Universität Leipzig entstand. Es wird auf die verwendeten Datenquellen und Technologien eingegangen und aufgetretene Probleme und deren Lösungen während der Entwicklung werden ebenso wie die Verwendung des finalen Tools dokumentiert.}

% renders abstract along with declaration at titlepage with \publishers workaroung
\publishers{%
    \normalfont\normalsize%
    \vspace{2cm}
    \parbox{\linewidth}{\centering\vspace{5cm}\kurz}\vfill
    \footnotesize
}

\begin{document}
\maketitle\clearpage
\thispagestyle{empty}
\tableofcontents\clearpage
\pagenumbering{arabic}

% %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% START YOUR DOCUMENT HERE
% %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

\section{Motivation}
\label{sec:motivation}

Der Schusswaffenbesitz nimmt in den USA im Vergleich zu anderen Nationen eine besondere Rolle ein. So ergab eine Befragung\footnote{\url{https://de.statista.com/statistik/daten/studie/28081/umfrage/us-buerger-zum-vorhandensein-einer-schusswaffe-in-ihrem-haushalt/}} aus dem Jahr 2017, dass es in 42\% der amerikanischen Haushalte mindestens eine Schusswaffe gab. Gleichzeitig sticht die USA bei Zahlen zu Waffenmissbrauchsfällen im internationalen Vergleich deutlich heraus. So wurden im Jahr 2010 beispielsweise 102 Tötungsdelikte pro 1 Millionen Menschen gezählt, das ist das 10-fache der Anzahl an Tötungsdelikten in Deutschland im selben Jahr\footnote{\url{http://edition.cnn.com/2017/10/03/americas/us-gun-statistics/index.html}}. Diese Sonderrolle der USA im Bezug auf Waffenbesitz und -missbrauch macht die Untersuchung der Hintergründe und möglicher Erklärungen zu einer interessanten Frage.\\ \\
Das Ziel des als 'BIM' betitelten Tools ist es daher, Informationen verschiedener Datenquellen so zu kombinieren, dass ein Nutzer sich, ausgehend von einem einzelnen Schusswaffenattentat, weitreichend zu dessen Umständen und Hintergründen, wie beispielsweise dem Waffenbesitz, informieren und dieses in lokalen, zeitlichen und rechtlichen Kontexten explorieren kann. In den USA regeln die lokalen Gesetze der einzelnen Staaten die Rechtslage zu Waffenbesitz und -verwendung individuell. Daher soll BIM es ermöglichen, die individuelle Situation der einzelnen Staaten zu untersuchen. Zusätzlich zu der räumlichen Aufteilung soll durch die Ermöglichung der Eingrenzung nach Jahren das Untersuchen einer zeitlichen Veränderung ermöglicht werden.\\


\section{Datenquellen}

\subsection{Verwendete Datenquellen}
\label{sec:vDatenquellen}

Das Mapping zwischen den zweistelligen Staatenkürzeln und den entsprechenden vollständigen Staatennamen wurde als csv-Datei aus der JVectormap-Dokumentation\footnote{\url{http://jvectormap.com/maps/countries/usa/}} entnommen.\\
    
Eine Übersicht über die Gesetzeslage für alle einzelnen Staaten liefert die (englische) Wikipedia. Es existiert sowohl eine Übersicht mit tabellarischen Zusammenfassungen\footnote{\url{https://en.m.wikipedia.org/wiki/Gun_laws_in_the_United_States_by_state}} der Rechtslage in den einzelnen Staaten, als auch ein weiterführender Artikel pro Staat.\\

Die vom FBI veröffentlichten Statistiken zu den Hintergrundprüfungen bei Waffenkäufen\footnote{\url{https://www.fbi.gov/file-repository/nics_firearm_checks_-_month_year_by_state.pdf}} liegen für den Zeitraum vom 30.11.1998 bis 30.09.2017 monatsweise vor. Sie umfassen keine Daten zum tatsächlichen Schusswaffenbesitz und entsprechend der Verweise im Datensatz ist eine 1-zu-1-Beziehung zwischen den Prüfungen und dem Waffenbesitz, basierend auf den unterschiedlichen Rechtslagen in den jeweiligen Staaten, \textit{nicht} möglich. Unserer Meinung nach stellen diese Daten jedoch zumindest eine grobe Übersicht dar und haben somit eine ausreichende Relevanz für die Darstellung in BIM.\\
    
Der Stanford-MSA-Datensatz\footnote{\url{https://library.stanford.edu/projects/mass-shootings-america}} vom 14.06.2016 enthält Informationen zu einzelnen Schusswaffenattentaten. Er liegt als csv-Datei vor und umfasst Daten der Jahre 1966 bis 2016. Stanford-MSA definiert ein \textit{Mass Shooting} als Schusswaffenattentat mit mindestens drei Opfern. Der Täter wird hierbei nicht mitgezählt und es ist im Rahmen dieser Definition nicht relevant, ob die Opfer getötet oder verletzt wurden. Diese Definition wurde als sinnvoll, und sich ihr daher im Rahmen der Entwicklung von BIM angeschlossen.\\
Es ist wichtig, anzumerken, dass der von Stanford veröffentlichte Datensatz keinen Anspruch an Vollständigkeit hat, sondern den Fokus vielmehr auf eine gute Quellenlage und Informationsaufbereitung setzt. So umfasst er detailliertere Informationen als alternative Datenquellen und jeder Eintrag erhält Verweise auf die Belege. Dies ermöglicht BIM die Bereitstellung qualitativ hochwertiger Informationen zu einzelnen Attentaten, statt einen quantitativen und repräsentativen, aber eventuell schlecht belegten Überblick zu ermöglichen. Da sich dies mit dem in Kapitel \ref{sec:motivation} definierten Ziel von BIM deckt, wurde dieser Datensatz den, in Kapitel \ref{sec:NichtVerwendeteDatenquellen} erwähnten, Alternativen vorgezogen. \\

Die New-York-Times stellt online ein Archiv ihrer Artikel seit 1851 zur Verfügung. Der Zugriff ist sowohl über eine Suchmaske\footnote{\url{https://query.nytimes.com/search/sitesearch/\#/}} als auch über eine API\footnote{\url{https://developer.nytimes.com/}} möglich. Aufgrund des einfacheren Zugriffs und der höheren Stabilität bei Änderungen an der Oberfläche der Archivseite wurde zugunsten der Nutzung der API entschieden. Diese nimmt http-Anfragen entgegen und liefert ein JSON-Objekt zurück, welches alle relevanten (Meta-)Daten enthält, darunter Titel, Datum, Abstract und Autor(en).\\

Ursprünglich angedacht war eine Live-Abfrage von Nachrichten in Reaktion zu Nutzeraktivitäten, doch dieser Ansatz wurde aus zwei Gründen nicht weiter verfolgt: Einerseits handelt es sich bei den gesammelten Nachrichten ausschließlich um historische Daten, welche sich (höchstwahrscheinlich) nicht mehr ändern werden. Eine kontinuierliche Wiederabrufung ist deshalb eher unnötig. Andererseits beschränkt die API die möglichen Anfragen eines einzelnen API-Key auf ein bestimmtes Kontingent, sowie eine Anzahl von Anfragen pro Sekunde. Aus diesen Gründen werden die Nachrichtenartikel stattdessen durch eine Servermethode komplett für alle Shootings angefragt und gespeichert. Die Webapp muss dann die benötigten Newsdaten nur noch auslesen.\\

In Hinblick auf Diversifizierung der dargebotenen Nachrichten wurde außerdem eine zweite Quelle herangezogen: \textit{The Guardian}. Dieser bietet zum einen eine (aus US-Sicht) ausländische, und dadurch eventuell alternative, Presseperspektive, sowie ebenfalls eine gut nutzbare API für einfache Anfragen. Die Beschränkungen der API sind dabei sehr ähnlich derer der NYT und auch hier erfolgt deshalb aus den oben genannten Gründen eine einmalige komplette Extraktion aller relevanter Artikel, statt kontinuierlicher Liveanfragen. \\
Zu beachten ist jedoch, dass mit der API des \textit{Guardian} keine Abstracts zurückgeliefert werden. Diese werden deshalb in einem zusätzlichen Schritt direkt von der Webseite extrahiert. Dazu wird mithilfe der Artikel-URL das entsprechende HTML-Dokument nach den relevanten Textstellen durchsucht, welche sich durch eine bestimmte DIV-Klassenhierarchie auszeichnen. Allerdings haben nicht alle Artikel des \textit{Guardian} solche Abstracts, weswegen auch in der Webapp teilweise nur Titel angezeigt werden.\\


\subsection{Nicht verwendete Datenquellen}
\label{sec:NichtVerwendeteDatenquellen}
    
Der auf demographicdata.org gefundene Datensatz zum Waffenbesitz\footnote{\url{http://demographicdata.org/facts-and-figures/gun-ownership-statistics/}} beschreibt, im Gegensatz zu dem von uns verwendeten Datensatz zu NICS-Hintergrundprüfungen, den tatsächlichen Waffenbesitz. Diese Daten liegen jedoch leider nur für das Jahr 2017 vor. Da BIM jedoch, wie in Kapitel \ref{sec:motivation} beschrieben, einen Fokus auf die jahresweise Untersuchung über einen längeren Zeitraum legt, konnte dieser Datensatz nicht verwendet werden.\\

Der auf \textit{demographicdata.org} gefundene Datensatz zu Schusswaffenattentaten in den USA zwischen 1966 und 2017\footnote{\url{https://www.kaggle.com/zusmani/us-mass-shootings-last-50-years/data}} weist zwar mehr Einträge auf als der von uns verwendete Datensatz von Stanford, er bietet jedoch weniger Informationen zu den einzelnen Einträgen und die Quellenlage ist nicht so umfassend dokumentiert wie dies im Stanford-MSA-Datensatz der Fall ist. Gleiches gilt auch für das GunViolenceArchive\footnote{\url{http://www.gunviolencearchive.org/reports/mass-shooting}}. Hier kommt noch hinzu, dass die Daten nicht in vollem Umfang öffentlich zur Verfügung stehen. Daher wurde gegen die Verwendung dieser beiden Quellen entschieden.\\


\section{Verwendete Technologien}

\subsection{Preprocessing}

Für die statischen, einmaligen Datenextraktionen wurde Python\footnote{\url{https://www.python.org/}} verwendet. Die einzelnen Verarbeitungsschritte für die jeweiligen Datensätze werden im Folgenden erläutert.\\

\textit{Übersicht zur Rechtslage aus der Wikipedia}\\
\\
Links zu den detaillierteren Einzelseiten der jeweiligen Staaten in der Wikipedia wurden ebenso wie Fußnoten zu den Quellen, wie beispielsweise den originalen Gesetzestexten selbst, beibehalten und sind damit auch während der Nutzung von BIM verfügbar. Informationen die nicht einzelnen Staaten zugeordnet werden können, sowie style- und Javascript-Tags wurden aus dem HTML-Code entfernt. Die Extrahierung des relevanten Inhalts ist sowohl in eine csv- als auch in eine js-Datei möglich. \\

\textit{Stanford-MSA-Datensatz zu Schusswaffenattentaten}\\
\\
Die Daten zu den Schusswaffenattentaten können wahlweise in eine csv- oder eine js-Datei extrahiert werden. Gleichzeitig zu der Reduzierung der Attribute auf die für BIM relevanten Informationen wird eine Filterung anhand des Datums vorgenommen. Während die 307 Einträge der Originaldatensatzes aus dem Zeitraum von 1966 bis 2016 stammen, enthält der für BIM erzeugte Datensatz lediglich die 234 Einträge für den Zeitraum ab 1999.\\

\textit{Staaten-Dictionary}\\
\\
Die csv-Datei wird als Dictionary eingelesen und ermöglicht die Zuordnungen von gewonnenen Daten zu den einzelnen Staaten. Dies ist an den verschiedenen Stellen des Extraktionsprozesses an denen nur jeweils eine der beiden Informationen gegeben ist notwendig. Ein Beispiel stellt die Extraktion der rechtlichen Lage dar. Die Rohdaten aus der Wikipedia erhalten lediglich den vollständigen Namen des Staates, für die Einbindung der Daten in BIM werden jedoch die Staatencodes benötigt. \\

\textit{New-York-Times Archiv}\\
\\
Für die einzelnen Schusswaffenattentate werden Anfragen an die Article-API der New-York-Times gesendet. Da zu erwarten ist, dass für die \textit{Mass Shootings} in weiter Vergangenheit keine zusätzlichen Nachrichten hinzukommen (vor allem, da ausschließlich ein Zeitraum von zwei Tagen nach dem Attentat betrachtet wird) und einzelne \textit{Mass Shootings} häufiger, auch von verschiedenen Nutzern, ausgewählt werden, ist eine statische Abfrage dieser Nachrichten sinnvoll. Zumal die dynamische Verarbeitung im Test nicht in ausreichender Geschwindigkeit durchgeführt werden konnte und es dadurch zu Verzögerungen im Frontend kam. Diese Wartezeiten waren zwar nur kurz, hatten jedoch einen beträchtlichen negativen Einfluss auf die User-Erfahrung. Da der Inhalt außerdem nicht zu umfangreich ist, um serverseitig komplett gespeichert werden, wurde zugunsten einer statischen Extraktion entschieden.\\Als Suchanfrage wird der Name der Stadt sowie der Name des Eintrags im Stanford-MSA-Datensatz im zeitlichen Rahmen von zwei Tagen nach dem Attentat verwendet. Aus der JSON-formatierten Antwort der API zur http-Anfrage wird ein HTML-Inhalt erzeugt und an die GUI ausgeliefert. Die aufbereiteten Antworten der API werden aus Effizienzgründen in einem Cache gespeichert.\\

\textit{The Guardian Open Platform}\\
\\
Die Anfragen und Verarbeitung der Nachrichtenobjekte der \textit{Guardian Open Platform} verhalten sich sehr ähnlich zu denen an die NYT-API. Signifikantester Unterschied sind die fehlenden Abstracts im zurückgelieferten JSON-Objekt. Deshalb werden, wie bereits erwähnt, diese direkt von den HTML-Seiten der einzelnen Artikel extrahiert, mithilfe einfacher XPath-Ausdrücke. Dies könnte in Zukunft allerdings Nachbearbeitung erfordern, falls der Guardian seine Seitenstruktur in dieser Hinsicht signifikant ändern sollte.\\  

Die relevanten Programme werden im folgenden kurz aufgelistet (alle Programme liegen dabei im Unterverzeichnis \textit{preprocessing}):\\
\begin{itemize}
\item \textit{ShootingsExtractor.py} - Extrahiert die Stanford-MSA-Daten zu Schusswaffenattentaten.
\item \textit{NewsExtractor.py} - Sammelt Nachrichtendaten zu Schusswaffenattentaten von APIs der NYT und des Guardians.
\item \textit{StateCodeDictionary.py} - Liefert Sammlung aus Staatenkürzeln und -namen.
\item \textit{LawExtractor.py} - Sammelt für alle US-Staaten die in der englischen Wikipedia ausgeschriebenen Sammlungen von Schusswaffengesetzen.
\item \textit{PopulationExtractor.py} - Extrahiert Bevölkerungsdaten pro Staat und Jahr aus einer Datei des Census-Archivs.
\item \textit{NICSExtractor.py} - Extrahiert Daten zu Hintergrundprüfungen aus einer NICS-Datei.
\item \textit{KeywordExtractor.py} - Extrahiert Keywords aus den gesammelten Nachrichtendaten mithilfe des RAKE-Algorithmus. Details hierzu in Kapitel \ref{sec:textanalyse}.
\end{itemize}


\subsection{Backend}

Im Backend ermöglichen die im vorigen Abschnitt erwähnten Python-Scripts die erneute Extraktion der ursprünglichen Datenquellen in größeren zeitlichen Abständen, um mögliche Updates der Datensätze zu adressieren. Dies betrifft die Daten bezüglich der \textit{Mass Shootings} und den damit zusammenhängenden Nachrichten aus dem Archiv der New York Times und des Guardians ebenso wie die Daten zu den NICS-Backgroundchecks und das Crawlen der Übersicht über die Rechtslage aus der Wikipedia.
\subsection{Frontend}

Für das Frontend werden HTML, CSS und Javascript verwendet, letzteres ergänzt durch JQuery\footnote{\url{https://jquery.com/}} sowie JScrollPane\footnote{\url{http://jscrollpane.kelvinluck.com/}} für browserübergreifende Designanpassungen.\\
JVectorMap\footnote{\url{http://jvectormap.com}} ermöglicht die verlustfrei skalierbare Darstellung der Karte der USA durch die Verwendung einer SVG-Datei, das Einfärben der einzelnen Staaten basierend auf der Anzahl der NICS-Hintergrundprüfungen sowie das Darstellen der Marker die die Schusswaffenattentate repräsentieren.\\


\subsection{Schnittstelle}
\label{sec:schnittstelle}

Um dynamisch das Python-Skript für die API-Anfrage des New-York-Times Archivs zu starten und das Caching der Ergebnisse durchführen zu können wird Flask\footnote{\url{http://flask.pocoo.org/}} verwendet, ein leichtgewichtiges Python-Framework für die Webentwicklung. Dies ist notwendig, da Javascript lokal, Python-Skripte jedoch auf dem Server ausgeführt werden. Für diese Schnittstelle beim Update der extrahierten Daten wird eine asynchrone Datenübertragung zwischen dem Browser und dem Server mittels AJAX\footnote{\url{https://de.wikipedia.org/wiki/Ajax_(Programmierung)}} genutzt, um durch die Verarbeitung im Backend nicht das Frontend zu Blockieren.

\subsection{Organisation und Formales}

Für die koordinierte und dokumentierte Entwicklung von BIM wurde das Versionskontrollsystem Git\footnote{\url{www.git-scm.com}} verwendet. Das entsprechende Repository wurde auf Gitlab\footnote{\url{www.gitlab.com}} angelegt. Diese Dokumentation wurde unter Verwendung von \LaTeX\footnote{\url{https://www.latex-project.org/}} erstellt. \\

\section{Textanalyse}
\label{sec:textanalyse}

Die Analyse von Texten ist ein zentraler Bestandteil des Felds der automatischen Sprachverarbeitung. Eine wesentliche Rolle hierbei spielen sogenannte \textit{Keywords}, Ein- oder Mehrwortphrasen, die signifikant häufiger in einem Text auftreten, als es bloßer Zufall erlauben würde, und dadurch tendenziell zentrale semantische Inhalte des Texts widerspiegeln können. \\
Für das Projekt wurde eine solche Keyword-Analyse für die gesammelten Nachrichtentexte daher in einem überschaubaren Rahmen ebenfalls angestrebt. Dafür wurde die Python-Bibliothek \textbf{rake-nltk} \footnote{https://github.com/csurfer/rake-nltk} verwendet, welche wiederum den sogenannten \textit{RAKE}-Algorithmus von Rose et al. implementiert\footnote{Rose, Stuart \& Engel, Dave \& Cramer, Nick \& Cowley, Wendy. (2010). Automatic Keyword Extraction from Individual Documents. Text Mining: Applications and Theory. 1 - 20.}. Gewählt wurden Algorithmus bzw. Bibliothek vor allem auch aufgrund ihrer schnellen und unkomplizierten Einsetzbarkeit, sowie ihrer Unabhängigkeit gegenüber anderen Bibliotheken und externer Daten.  


\subsection{RAKE}

Die automatische Extraktion von Keywords ist noch immer ein aktives Forschungsfeld, da die Aufgabenstellung alles andere als trivial ist und in der Vergangenheit hauptsächlich manuell durch professionelle Kuratoren durchgeführt wurde. \textit{RAKE} ist ein Keyword-Extraktionsalgorithmus auf Dokumentlevel und basiert auf der Annahme, dass Keywords oft aus mehreren Einzelworten bestehen, jedoch nur selten Stoppworte oder Satzzeichen enthalten. \\

\textit{RAKE} erhält als Input Phrasen- und Worttrenner sowie eine Stoppwortliste für die betreffende Sprache. Letztere ist für Englisch in der genutzten Pythonbibliothek bereits vorhanden. Anhand dieser Eingaben wird zunächst eine Menge von Keyword-Kandidaten erzeugt und ein Kookkurrenzgraph für Kookkurrenzen innerhalb dieser Kandidaten-Phrasen aufgebaut. Dann wird für jeden Kandidaten ein Score errechnet, der sich aus der Summe der Scores ihrer Einzelworte zusammensetzt. Diese wiederum errechnen sich aus mehreren Metriken, basierend auf Grad und Häufigkeit von Wortknoten im Kookkurrenzgraph. Genauer sind dies Häufigkeit \textit{freq(w)}, Grad \textit{deg(w)} und Verhältnis von Grad zu Häufigkeit \textit{freq(w)}/\textit{freq(w)}. Die Top-N scorenden Kandidaten werden dann als Keywords ausgegeben. 

\subsection{Implementierung und Ergebnisse}

Die gesamte Pipeline der Keyword-Analyse wird in der Datei \textit{KeywordExtractor.py} abgehandelt. Hierfür kann aus zwei wesentlichen Varianten gewählt werden: Keywords können entweder ausschließlich in den Abstracts, oder in den gesamten Volltexten der Artikel extrahiert werden. Für die zweite Variante werden dafür mit Hilfe der gesammelten Artikel-URLs die Texte direkt von den Webseiten gecrawlt. Auf den kombinierten Textkorpus aus Abstracts oder Vollartikeln wird dann der RAKE-Algorithmus angewandt, welcher eine spezifizierbare Anzahl von besten Kandidaten inklusive deren Scores zurückgibt. Für die Implementierung des Projekts wurden 200 als Anzahl festgelegt. Die maximale Länge der Phrasen ist ebenfalls bestimmbar, der für das Projekt gewählte Wert beträgt 2. Diese Kandidaten werden dann, sofern vorhanden, in den auf der Webseite aufgelisteten Titeln und Abstracts hervorgehoben, mit der zugehörigen Score als Hovertext. Außerdem wird eine sortierte Liste erstellt, die ebenfalls im Portal im News-Tab über den Button \textit{Key Phrases} eingesehen werden kann. \\


\begin{table}[H]
\begin{center}
\begin{tabular}{ l | r }
    \textbf{Keyphrase} & \textbf{Score}	\\	
      \hline	
	dont love & 10.917 \\
	korean spa & 9.5 \\
    works better & 8.033 \\
  \hline			
\end{tabular}
\caption {Negative Beispiele der Phrasenextraktion \label{table:neg_pe}}
\end{center}
\end{table}

\begin{table}[H]
\begin{center}
\begin{tabular}{ l | r }
    \textbf{Keyphrase} & \textbf{Score}	\\	
      \hline
    called police & 11.51 \\	
	fatally shot & 11.139 \\
    deadly rampage & 10.667 \\
    latest shooting & 10.57 \\
  \hline			
\end{tabular}
\caption {Positive Beispiele der Phrasenextraktion \label{table:pos_pe}}
\end{center}
\end{table}


Für das Projekt wurden die Keyword-Kandidaten ausschließlich aus den Abstracts gewählt. Grund hierfür waren vorangehende Experimente: Bei einer Suche über die Volltexte lagen fast alle Kandidaten außerhalb der Titel und Abstracts, was sie für die Zwecke des Portals durch ausbleibende Hervorhebungen leider weniger nutzbar machte. Die Gesamtliste bietet allerdings dennoch eine interessante Informationsquelle.\\
Die Qualität der extrahierten Keywords schwankt stark. Viele eher zufällig wirkende Phrasen lassen wenig Aufschluss über semantische Inhalte zu, wie in Tabelle \ref{table:neg_pe} beispielhaft aufgeführt. Top-200-Scores befanden sich dabei in einem Intervall zwischen 17.511 und 6.295.\\
Es gab allerdings auch viele positive Extraktionen, wie Beispiele in Tabelle \ref{table:pos_pe} zeigen. Insgesamt könnte der Prozess sicherlich noch weiter verbessert werden, besonders mit der Erprobung weiterer, alternativer Algorithmen, größerer Datengrundlagen und statistischer Maße. Für den Umfang dieses Projektes ist es allerdings bereits eine interessante Betrachtung und gibt einen Hinweis auf den Nutzen, verschiedenste Bereiche von NLP zu verknüpfen, um dargebotene Informationen für Nutzer weiter anreichern zu können.

\section{Verwendung von BIM}

\subsection{Anwendungsbezogene Aspekte}

\begin{figure}[h]
\centering
\includegraphics[width=1.0\textwidth]{bim_screenshot.png}
\caption{Screenshot der entstandenen Website für die Detailansicht eines Shootings}
\label{fig:screenshot_bim_detailsView}
\end{figure}


Abbildung \ref{fig:screenshot_bim_detailsView} zeigt einen Screenshot der GUI von BIM. Im linken Bereich ist die Karte für die Shooting-Auswahl zu sehen. Die GUI Elemente an den Rändern der Karte ermöglichen das Filtern des zeitlichen Intervalls der angezeigten Shootings, das Ausblenden aller Shootings sowie das Zoomen der Karte. Letzteres ist auch durch die Verwendung des Scrollrades der Maus möglich. Die Staaten sind auf der Karte entsprechend der Anzahl der Backgroundchecks pro Einwohner eingefärbt. Diese Einfärbung passt sich entsprechend der Auswahl der Jahre durch den Slider an. Die Legende unten links ermöglicht eine grobe Einordnung der Werte, auf denen die Einfärbung der Staaten basiert.\\

\noindent
Im rechten Bereich ist die Detailansicht für ein auf der Karte ausgewähltes Shooting zu sehen. Abgesehen von dieser Übersicht stehen für den rechten Bereich der GUI eine Liste der gefundenen Nachrichtenartikel zu dem ausgewählten Shooting sowie eine Übersicht über die aktuelle Rechtslage des Staates in dem das Shooting stattfand zur Verfügung. Im unteren rechten Bereich befinden sich zwei Buttons für die manuelle Anpassung der Schriftgröße sowie ein Info-Button mit dem sich die Hintergrundinformationen zu den dargestellten Daten anzeigen lassen.\\

\noindent
Die im Rahmen des Praktikums erstellte GUI ermöglicht das interaktive Untersuchen der einzelnen Shootings und der damit verknüpften weiteren Daten. Die Aggregation der Informationen zur Rechtslage, der Anzahl der Backgroundchecks sowie der historischen Nachrichten führt zu einer breiten Datenbasis, die vielfältige und tiefe Explorationen ohne eine eigenen Recherche möglich macht.


\subsection{Technische Aspekte}

Für die lokale Verwendung ohne echten Webserver ist es notwendig, dass Python und Flask installiert sind. Die Notwendigkeit von Flask wird in Kapitel \ref{sec:schnittstelle} erläutert. Sobald diese Bedingungen gegeben sind muss lediglich der Server im Hauptverzeichnis des Projekts gestartet werden und im Browser der Localhost auf Port 5000 aufgerufen werden. Ein Online-Einsatz des Tools wäre selbstverständlich auch möglich. Hierfür wäre es lediglich notwendig, den Server minimal anzupassen. \\


\section{Aufgetretene Probleme}


Das größte Problem der Arbeit trat schon in der Definitionsphase der Zielstellung des Projekts auf und konnte daher schon von Beginn an berücksichtigt werden: Während nach dem ersten Entwurf der Zielstellung Schusswaffenmissbrauch allgemein untersuchbar gemacht werden sollte, stellte sich schnell heraus, dass entsprechende Datenquellen zur gesamten Schusswaffengewalt mit hohen Qualitätsstandards leider nicht oder zumindest nicht in vollem Umfang zur Verfügung standen. Daher legt diese Arbeit ihren Fokus auf \textit{Mass Shootings}.\\
    
Zudem benutzen verschiedene Quellen zum Teil auch unterschiedliche Definitionen für \textit{Mass Shootings}. Oft variiert beispielsweise der Schwellwert ab dem nicht mehr von einem \textit{Shooting} sondern von einem \textit{Mass Shooting} gesprochen wird und während in einigen Quellen der oder die Täter, sofern er oder sie auch starben oder verletzt wurden, zu den Opfern gezählt werden, wird dies in anderen Quellen getrennt oder gar nicht berücksichtigt. Das aus unterschiedlichen Definitionen entstehende Problem wurde für dieses Projekt gelöst, indem von Anfang an, wie in Kapitel \ref{sec:vDatenquellen} beschrieben, die Definition des Stanford-MSA Datensatzes als Standard festgelegt wurde.\\
    
In einer frühen Version von BIM trat das Problem auf, dass der Ladevorgang der Nachrichtenartikel zu einem auf der Karte ausgewählten \textit{Mass Shooting} zu langsam geladen und somit die GUI blockiert wurde. Dies konnte durch die Vermeidung des dynamischen Abrufens der News-API behoben werden. Daraus entstehen keine Nachteile beim Informationsumfang, da die relevanten Artikel einmalig über das Erneuern der Crawling-Prozesse durch den Server aktualisiert werden können. \\

Die Probleme der Unabhängigkeit des Designs und der Informationsvisualisierung von der Verwendung verschiedener Browser, wurde durch das Verwenden eines relativen Layouts und der Einbindung verschiedener js-Plugins adressiert. JVectorMap sorgte vor allem durch eine sehr spärliche, unvollständige Dokumentation für Schwierigkeiten. Unklarheiten bei der Implementierung der Karte mussten somit durch zeitaufwendigeres Ausprobieren gelöst werden, wohingegen eine gute Dokumentation deutlich schneller Lösungen hätte liefern können.

\section{Fazit \& Ausblick}

Das Projekt bietet Nutzern eine Plattform zur interaktiven Exploration verschiedener kombinierter Datenquellen zum übergreifenden Thema des Schusswaffenmissbrauchs in den USA. Es liefert ein Beispiel für die Relevanz von Wissens- und Contentmanagement als Informationsaggregation und -aufbereitung für einen benutzerfreundlichen Zugriff auf den Inhalt, sowie potentiellen Wissensgewinn durch die Verknüpfung vorher separierter Datenquellen.\\

Sinnvolle Erweiterungen wären besonders im Bezug auf die Nachrichtendaten denkbar. Hier könnten einerseits weitere Quellen herangezogen werden, da momentan einigen Vorfällen noch keine Nachrichtenmeldungen zugeordnet werden können. Dafür müssten eventuell lokale Nachrichtendienste berücksichtigt werden, oder allgemeinere Abfragen mithilfe von Suchmaschinen miteinbezogen werden. Dabei sollte allerdings auch auf die Qualität der extrahierten Artikel geachtet werden, ein weiterer Punkt der auch im bestehenden Projekt sicher noch verbessert werden kann: so liefern die News-APIs momentan stellenweise auch irrelevante Meldungen, sodass eine Filterung die Gesamtmenge weiter verbessern könnte.\\
Wie auch in Kapitel \ref{sec:textanalyse} erwähnt, ist die Keyword-Extraktion ebenfalls ausbaubar, sei es durch alternative Algorithmen oder eine größere Datengrundlage.\\
Zuletzt sollten natürlich auch die anderen Datengrundlagen des Projekts möglichst aktuell gehalten werden - nicht zuletzt, da dieses Thema auch in der Gegenwart nicht ans Brisanz und Relevanz eingebüßt hat.



% %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% HERE ENDS YOUR DOCUMENT
% %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

% Settings for the references (which are numbered in roman style)
\label{LastPageBeforeRefs}
\clearpage
\ofoot{\vspace{-0.1cm}\textbf{\large\thepage}/\textbf{\large\getpagerefnumber{LastBibPage}}}
\pagenumbering{Roman}
\printbibliography
\label{LastBibPage}
\end{document}
