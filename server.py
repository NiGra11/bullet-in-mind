from flask import Flask, render_template
from flask import request
import json

app = Flask(__name__)

@app.route('/')
def index():
  return render_template('/bullet-in-mind.html')

if __name__ == '__main__':
  app.run(debug=True)
